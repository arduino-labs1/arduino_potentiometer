#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>

//               0   1    2    3   4   5    6    7    8    9  .
int codes[] = {222, 10, 230, 110, 58, 124, 252, 14, 254, 126, 1};

const int pinSelect = 8;

void transfer (int num) {
        SPDR = num;
        while (!(SPSR&(1<<SPIF)));
        PORTB |= (1<<PORTB2);
        PORTB &= ~(1<<PORTB2);
}

void segment_number (int num) {
        transfer(codes[num]);
}

void adc_init() {
        ADCSRA |= (1<<ADEN)|(1<<ADPS1)|(1<<ADPS2)|(1<<ADPS0);
        ADMUX |= (1<<REFS1)|(1<<REFS0)|(1<<ADLAR);
}

void display_init() {
        DDRB |= ((1<<PORTB2)|(1<<PORTB3)|(1<<PORTB5));
        PORTB &= ~((1<<PORTB2)|(1<<PORTB3)|(1<<PORTB5));
        SPCR = ((1<<SPE)|(1<<MSTR));
        SPDR = 0b00000000;
        while (!(SPSR&(1<<SPIF)));
        PORTB |= (1<<PORTB2);
        PORTB &= ~(1<<PORTB2);
        _delay_ms(100);
}

unsigned int adc_convert () {
        ADCSRA |= (1<<ADSC);
        while((ADCSRA & (1<<ADSC)));
        return (unsigned int) ADC;
}

void setup() {
        display_init();
        adc_init();
}

int main() {
        unsigned int adc_value;
        setup();
        while (1) {
                adc_value = adc_convert();
                segment_number(adc_value / 7000);
                _delay_ms(100);
        }
        return 0;
}

