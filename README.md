# arduino_potentiometer

The output resistance of the potentiometer with Arduino Nano and SN74HC595N

## Content
* Arduino Nano
* Shift Register SN74HC595N
* Seven-segment display
* Potentiometer
* Resistor  x8
* Connecting wires

## Run
```bash
$ avr-gcc -g -O1 -mmcu=atmega328p -o main.elf main.c
$ avrdude -v -patmega328p -c arduino -P /dev/ttyUSB0 -U flash:w:'main.elf'
```
